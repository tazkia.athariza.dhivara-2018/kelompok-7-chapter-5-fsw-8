// creating server with express and also using ejs as view engine
const express = require("express");
const expressLayouts = require("express-ejs-layouts");
const { loadData } = require("./utils/data");
const app = express();
const PORT = process.env.PORT || 8000;

app.use(express.urlencoded());
app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static(__dirname + "/views"));

app.get("/", (req, res) => {
  const data = loadData();
  res.render("index", {
    layout: "layouts/main-layouts",
    title: "Home",
    data,
  });
});

app.get("/buy", (req, res) => {
  res.render("buy", {
    layout: "layouts/main-layouts",
    title: "Buy",
  });
});
app.get("/register", (req, res) => {
  res.render("register", {
    layout: "layouts/main-layouts",
    title: "Register",
  });
});

app.use((req, res) => {
  res.status(404).render("404", {
    layout: "layouts/main-layouts",
    title: "404",
  });
});

app.listen(PORT, () => {
  console.log(`Server berjalan di http://localhost:${PORT}`);
});

const fs = require("fs");

const loadData = () => {
  const fileBuffer = fs.readFileSync("./data/data.json", "utf-8");
  const data = JSON.parse(fileBuffer);
  return data;
};

module.exports = { loadData };
